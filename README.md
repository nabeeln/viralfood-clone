# Viral Food Website Clone

## v7
- signin signup page

## v6
- delivery / pickup area selection part

## v5
- incorporated redux with cart sections 

## v4
- View cart linking
- View cart page design
- view cart button in mobile view

## v3
- Basic Linking (Home,Branches,FAQ,individual food item page)
- card views - food section [ almost responsive ]
- individual food item page with passing hard coded array data

## v2
- Main Page Styling
- Almost Responsive Main Screen 
- Button Clicks in the banner
- Food Section Tab Navigation
- Food Section Card Views Using HardCoded Data 
- footer - WIP