import React from 'react'
import './ProductSearch.css'
import { ReactComponent as Placeholder } from '../images/search-placeholder.svg'
import { FoodCard } from '../components/FoodCard'
import {useSelector} from 'react-redux'

export default function ProductSearch(props){
    const searchQuery = props.match.params.search;
    const foodData = useSelector(state => state.food.items)
    const filtered = foodData.filter( item => item.title.toLocaleLowerCase().search(searchQuery) >= 0)
    
    return (
       <div className='search-main'>
            {
                searchQuery == null ? <FoodCard data={foodData} /> : filtered.toString() !== '' ? <FoodCard data={filtered}/> : 
                (
                    <>
                        <Placeholder className='placeholder-image'/>
                        <h1>Sorry, Try Searching Different Dish</h1>
                    </>
                )
            }
       </div>
    )
}