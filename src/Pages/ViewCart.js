import React,{ Component } from 'react'
import { connect } from 'react-redux'
import './ViewCart.css'
import { updateItemThunk, deleteItemThunk } from '../redux/CartSlice'
import { Link } from 'react-router-dom'
import { ReactComponent as EmptyCart } from '../images/cart-empty.svg'

class ViewCart extends Component{
    render(){
        this.cart = this.props.state
        this.empty = (
            <div className='empty-cart'>
                <EmptyCart className='empty-cart-image'/>
                <div className='empty-cart-text'>
                    Cart is Empty
                </div>
            </div>
        )
        this.cartList = (
                this.cart.items.map(cartItem => {
                const id = cartItem.id
                const title = cartItem.itemName
                const count = cartItem.count
                const totPrice = cartItem.totPrice
                const image = cartItem.image
                return (
                    <div className='view-cart-list' key={id}>
                        <div className='view-cart-list-item'>
                            <div className='view-cart-list-image'>
                                <img alt={title}  src={image} />
                            </div>
                            <div className='view-cart-list-col1'>
                                <div className='view-cart-list-col2'>
                                        <div className='view-cart-list-title'>
                                            {title}
                                        </div>
                                        <div className='view-cart-list-count'>
                                            <i className="fas fa-plus-circle" onClick={()=>{
                                            // dispatch(updateItem({id:id,op:'inc'}))
                                            this.props.updateItemThunk({id:id,op:'inc'})
                                        }}></i>
                                            <div className='view-cart-list-items-count-no'>
                                                {count}
                                            </div>
                                            <i className="fas fa-minus-circle" onClick={
                                                ()=>{
                                                    // dispatch(updateItem({id:id,op:'dec'}))
                                                    this.props.updateItemThunk({id:id,op:'dec'})
                                                }
                                            }></i>
                                        </div>
                                </div>
                                <div className='view-cart-list-col3'>
                                    <div className='view-cart-list-remove' onClick={
                                            () => {
                                                // dispatch(deleteItem({id}))
                                                this.props.deleteItem({id:id})
                                            }
                                        }>
                                            Remove
                                    </div>
                                    <div className='view-cart-list-price'>
                                            KWD {totPrice}
                                    </div>
                                </div>
                            </div>
                    </div>
                    <hr/>
                </div>
            )
        })
    )

        return (
            <div className='view-cart'>
                {
                    this.cart.items !== '' ? <div className='view-cart-title'>
                                Cart
                          </div>:null
                }
                {/* <div className='view-cart-type'>
                    This is a Pickup Order
                </div> */}
                {
                    this.cart.items.toString() === '' ? this.empty:this.cartList
                }
                <div className='view-cart-buttons'>
                    <Link to='/' className='view-cart-buttons-cont-shop'>
                        Continue Shopping
                    </Link>
                    { this.cart.items.toString() !== '' ? <div className='view-cart-buttons-checkout'>
                        Checkout
                    </div>:null }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return { state: state.cart }
}

const mapDispatchToProps = dispatch => {
    return {
        updateItemThunk: (args) => dispatch(updateItemThunk(args)),
        deleteItem: (actionArg) => dispatch(deleteItemThunk(actionArg)) 
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(ViewCart)