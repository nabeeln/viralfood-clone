import React, {useState} from 'react'
import './Home.css'
import TabNav from '../components/TabNav'
import AddCart from '../components/AddCart'
import { Link } from 'react-router-dom' 
import { useSelector } from 'react-redux'

export default function Home(){
    const areas = useSelector( state => state.delivery_options )
    // handle delivery and pickup button clicks
    const [ option,setOption ] = useState(areas.defaultOption)
    //get selected area from Delivery Options page.
    const optionArea = option === 'Delivery' ? areas.delivery:areas.pickup
    
    return (
        <>
            <div className='banner'>
                <div className='banner-title'>
                    Taste of Kuwait
                </div>
                <div className='button-selector-group'>
                    <div 
                        className={ option === 'Delivery' ? 'button-selector-delivery-active':'button-selector-delivery' } 
                        onClick={ ()=> setOption('Delivery') }
                    >
                        Delivery
                    </div>
                    <div 
                        className={ option === 'Pickup' ? 'button-selector-pickup-active':'button-selector-pickup' } 
                        onClick={ ()=> setOption('Pickup') }
                    >
                        Pickup
                    </div>
                </div>
                <Link to={{pathname:'/delivery-options',state:{option:option}}} className='button'>
                    { optionArea !== '' ? optionArea:`Select ${option} Area` }
                </Link>
            </div>
            {/* food items section */}
            <TabNav/>
            {/* add cart section for mobile view */}
            <AddCart/>
        </>
    )
}