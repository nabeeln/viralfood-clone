import React,{useState} from 'react'
import './FAQ.css'
import { faq_data } from '../data/FAQdata'
import { ReactComponent as FaqImage } from '../images/faq.svg'

export default function FAQ(){

    const [ click,setClick ] = useState({open:false,question:''})
    const answer_height = React.useRef([])
    const answerHeight = (data,i) => { 
        click.open &&  console.log(answer_height.current[i].scrollHeight)
        return click.open && click.question === data.id  && answer_height.current ? {height:answer_height.current[i].scrollHeight }:{height:0} 
    }

    return (
        <div className='faq-main'>
            <FaqImage className='faq-image'/>
            <div className='faq-title'>
                FAQ
            </div>
            <div className='faq'>
                {
                    faq_data.map( (data,i) => {
                        return (
                            <div key={data.id} className='question-item'>
                                <div className='question-view' onClick={ () => setClick( click.question === data.id ? {open:false,question:''}:{open:true,question:data.id} ) }>
                                    <i className="fas fa-circle"></i>
                                    <div className='question'>
                                        {data.question}
                                    </div>
                                </div>
                                <div style={answerHeight(data,i)} className='answer-view'>
                                    {/* <div className='answer-line'/> */}
                                    <div ref={element => answer_height.current.push(element)} className='answer'>
                                        {data.answer}
                                    </div>
                                </div>
                            </div>                            
                        )
                    } )
                }
            </div>
        </div>
    )
}