import React from 'react'
import './Branches.css'
import { ReactComponent as BranchesImage } from '../images/branches.svg'
import { branches } from '../data/BranchesData.js'

export default function Branches(){
    return (
        <div className='branches-main'>
            <BranchesImage className='branches-image'/>
            <div className='branches-title'>
                Branches
            </div>
            <div className='branches-grid'>
                {
                    branches.map( branch => {
                        return (
                            <div className='branches-item'>
                                <div className='branches-item-title'>
                                    {branch.branch}
                                </div>
                                <div className='address-view'>
                                    <i className="fas fa-map-marker-alt"></i>
                                    <div className='address'>
                                        <div>
                                            Block: {branch.block} 
                                        </div>
                                        <div>
                                            Street: {branch.street}
                                        </div>
                                        <div>
                                            Area: {branch.area}
                                        </div>
                                        <div>
                                            Country:{branch.country}
                                        </div>
                                    </div>
                                </div>
                                <div className='phone-view'>
                                    <i className="fas fa-phone-alt"></i>
                                    <div className='phone'>
                                        {branch.phone}
                                    </div>
                                </div>
                            </div>
                        )
                    } )
                }
            </div>
        </div>
    )
}