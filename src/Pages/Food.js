import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './Food.css'
import { connect } from 'react-redux'
import { addToCartThunk, fetchCart } from '../redux/CartSlice'
import { addToWishList } from '../redux/FoodSlice'

class Food extends Component{
    constructor(props){
        super(props)
        this.state = { count: 1 }   
    }

    componentDidUpdate() {
        this.props.loadData()
    }

    render(){
        this.food = this.props.state
        this.foodData = this.food.find(item => item.id === this.props.match.params.id)
        this.food_id = this.foodData.id
        this.title = this.foodData.title
        this.des = this.foodData.des
        this.image = this.foodData.image
        this.favorite = this.foodData.favorite
        this.price = this.foodData.price
        this.totPrice = this.price * this.state.count

        this.incrementCount = () => this.setState({count: this.state.count + 1})
        this.decrementCount = () => this.state.count >  1 ? this.setState({count:this.state.count - 1}):null

        this.addItems = () => {
            this.props.addData({title:this.title,count:this.state.count,price:this.price,food_id:this.food_id,image:this.image})
        }
        this.updateFav = () => {
            this.props.updateFav({id:this.food_id,favorite:!this.favorite});
        }
        return (
            <div className='food-main'>
                <div className='food-image'>
                    <img alt={this.title} src={this.image} />
                </div>
                <div className='food-info'>
                    <div className="food-wish-group">
                        <div className='food-info-title'>
                            {this.title}
                        </div>
                        <div className="title-wish" onClick={this.updateFav}>
                            {
                                this.favorite ? <i className="fas fa-heart fa-lg"/>:<i className="far fa-heart fa-lg"/>
                            }
                        </div>
                    </div>
                    <div className='food-info-des'>
                        {this.des}
                    </div>
                    <div className='food-info-price'>
                        Special Price: KWD {this.price}
                    </div>
                    <div className='food-info-instruction'>
                        <input 
                            className='food-info-instruction-input' 
                            type='text'
                            placeholder='Add Instruction'    
                        />
                        <hr/>
                    </div>
                    <div className='food-info-count'>
                        <div  className='food-info-count-inc' onClick={this.decrementCount}>
                            -
                        </div> 
                        {this.state.count} 
                        <div  className='food-info-count-dec' onClick={this.incrementCount}>
                            +
                        </div>
                    </div>
                    <Link to='/' className='food-info-add' onClick={this.addItems}>
                        <div className='food-info-add-price'>
                            KWD {this.totPrice}
                        </div>
                        <div className='food-info-add-cart'>
                            Add to Cart
                        </div>
                    </Link>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return { state: state.food.items }
}

const mapDispatchToProps = (dispatch) => { 
    return {
        loadData: () => dispatch(fetchCart()),
        addData: (actionArgs) => dispatch(addToCartThunk(actionArgs)),
        updateFav: (actionArgs) => dispatch(addToWishList(actionArgs))
    } 
}
export default connect(mapStateToProps,mapDispatchToProps)(Food)