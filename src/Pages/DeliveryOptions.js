import React,{ useState } from 'react'
import './DeliveryOptions.css'
import { deliveryAreas, pickupAreas } from '../data/AreaData'
import { Link,useLocation } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { addArea } from '../redux/OptionsSlice'

export default function DeliveryOptions(){
    const location = useLocation()
    //area options selector buttons
    const [ button,setButton ] = useState(location.state.option)
    const dispatch = useDispatch()
    //get pickup areas and delivery areas based on the button clicked
    const option = button === 'Pickup' ? pickupAreas:deliveryAreas
    return (
        <div className='delivery-options-main'>
            <div className='delivery-options-title'>
                Select {button !== '' ? button: null} Area
            </div>
            <div className='delivery-options-buttons'>
                <div 
                    className={button==='Delivery' ? 'delivery-options-buttons-delivery-clicked':'delivery-options-buttons-delivery'}
                    onClick={()=>setButton('Delivery')}
                >
                    Delivery
                </div>
                <div 
                    className={button==='Pickup' ? 'delivery-options-buttons-pickup-clicked':'delivery-options-buttons-pickup'} 
                    onClick={()=>setButton('Pickup')}
                >
                    Pickup
                </div>
            </div>
            <div className='delivery-options-area-list'>
                {
                    option.map(area=>{
                        return (
                        <Link 
                            className='delivery-options-area-list-item' 
                            onClick={ () => dispatch(addArea({area:area,defaultOption:button})) }
                            to='/'
                        >
                            {area}
                            <hr/>
                        </Link>
                        )
                    })
                }
            </div>
        </div>
    )
}