import React from 'react'
import './WishList.css'
import { useSelector } from 'react-redux'
import {FoodCard } from '../components/FoodCard'
import { ReactComponent as EmptyList } from '../images/wishlist.svg'

export default function WishList(){
    const foodList = useSelector(state=>state.food.items)
    const wishList = foodList.filter(item=>item.favorite)
    const empty = (
        <div className='empty'>
            <EmptyList className='wish-empty'/>
            <h1>
                Wish list is Empty
            </h1>
        </div>
    )
    return(
        <div className='wishlist'>
            <div className="wishlist-title">
                Wish List
            </div>
            <div className="wishlist-items-card">
                {
                    wishList.length === 0 ? empty:<FoodCard data={wishList}/>
                }
            </div>
        </div>
    )
}