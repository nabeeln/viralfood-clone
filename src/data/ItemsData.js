export const ItemsData = [
    {
        id:'1',
        title: 'Meat Pizza',
        des: `Homemade thin crust pizza, topped off with two types of cheese, bacon, ham, 
              pepperoni and hot sausage! A must make for meat lover's.`,
        price: 150,
        categories: ['New','Pizza'],
        image:'/food/meat_pizza.jpg',
        favorite:false
    },{
        id:'2',
        title:'Black Bean Burgers',
        des:'Black Bean Burgers',
        price:500,
        categories:['Burger'],
        image:'/food/black_bean_burger.jpeg',
        favorite:false
    },{
        id:'3',
        title:'Butter Cake',
        des:'butter cake',
        price:852,
        categories:['Cakes'],
        image:'/food/butter.jpeg',
        favorite:false
    },{
        id:'4',
        title:'Pepparoni Pizza',
        des:`Pepperoni is an American variety of salami, made from a cured mixture of pork 
                and beef seasoned with paprika or other chili pepper. Pepperoni is characteristically 
                soft, slightly smoky, and bright red in color. Thinly sliced pepperoni is a popular 
                pizza topping in American pizzerias`,
        price:100,
        categories:['New','Featured','Best Sellers','Pizza'],
        image:'/food/pepparoni.jpg',
        favorite:false
    },{
        id:'5',
        title:'Black Forest Gateau.',
        des:'Black Forest Gateau.',
        price:556,
        categories:['New','Featured','Best Sellers','Cakes'],
        image:'/food/black_forest.jpeg',
        favorite:false
    },{
        id:'7',
        title:'Beef Burgers',
        des:'Beef Burgers',
        price: 205,
        categories:['Burger'],
        image:'/food/beef_burger.jpeg',
        favorite:false
    },{
        id:'8',
        title:'Portobello Mushroom Burgers',
        des:'Portobello Mushroom Burgers',
        price: 193,
        categories:['Burger'],
        image:'/food/portobello_burger.jpeg',
        favorite:false
    },{
        id:'9',
        title:'Black Bean Burgers',
        des:'Black Bean Burgers',
        price: 500,
        categories:['Burger'],
        image:'/food/black_bean_burger.jpeg',
        favorite:false
    }
]

export const Categories = [
    {
        id: 1,
        title:'New'
    },
    {
        id: 2,
        title:'Featured'
    },
    {
        id: 3,
        title:'Best Sellers'
    },
    {
        id: 4,
        title:'Cakes'
    },
    {
        id: 5,
        title:'Pizza'
    },
    {
        id: 6,
        title:'Burger'
    }
]
