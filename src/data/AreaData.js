export const deliveryAreas = [
    'Hawalli',
    'Jabriya',
    'Salwa',
    'Bayan',
    'Mubarak-Al-Abdullah-West-Mishr',
    'Siddiq',
    'Shaab',
    'Rumaithiya',
    'Hitteen',
    'Salam',
    'Zahra',
    'Al-Bedaa',
    'Mishrif',
    'Salmiya'
]

export const pickupAreas = [
    'Block :kuwait Salad Sabahiya kuwait Kuwait City',
    'Block :kuwait Salad Sabahiya 2 kuwait 2 Kuwait City',
    'Block :kuwait Salad Sabahiya 3 kuwait 3 Kuwait City'
]