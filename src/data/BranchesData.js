export const branches = [
    {
        branch: 'Main Branch',
        block:'Taste Bucket B1, a2 ',
        street:'Taste Bucket',
        area:'kuwait city',
        country:'kuwait',
        phone:'96598885895'
    },
    {
        branch: 'Kuwait City',
        block:'kuwait Salad Sabahiya',
        street:'kuwait ',
        area:'kuwait city',
        country:'kuwait',
        phone:'9874563214'
    },
    {
        branch: 'Kuwait City',
        block:'kuwait Salad Sabahiya 2',
        street:'kuwait 2',
        area:'Kuwait City',
        country:'Kuwait',
        phone:'9874563214'
    },
    {
        branch: 'Kuwait City',
        block:'kuwait Salad Sabahiya 3',
        street:'kuwait 3',
        area:'Kuwait City',
        country:'Kuwait',
        phone:'9874563214'
    }
]