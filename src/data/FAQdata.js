export const faq_data = [

    {
        id:0,
        question:'How do I cancel booking?',
        answer:`To help our restaurants manage their sittings we ask our diners to give at least 24 hours' notice of any cancellations. You can do this by calling your restaurant direct.`
    },
    {
        id:1,
        question: 'Are there any limitations to my deal?',
        answer: `You can find our general Terms and Condition elsewhere on this site.`
    }
        
        
]