import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import './ViewCart.css'
import { updateItemThunk, deleteItemThunk } from '../redux/CartSlice'
import { Link } from 'react-router-dom'
import { ReactComponent as EmptyCart } from '../images/cart-empty.svg'

export default function ViewCart(){
    const dispatch = useDispatch()
    const cart = useSelector(state => state.cart)
    const empty = (
        <div className='empty-cart'>
            <EmptyCart className='empty-cart-image'/>
            <div className='empty-cart-text'>
                Cart is Empty
            </div>
        </div>
    )
    const cartList = (
            cart.items.map(cartItem => {
            const id = cartItem.id
            const title = cartItem.itemName
            const count = cartItem.count
            const totPrice = cartItem.totPrice
            const image = cartItem.image
            return (
                <div className='view-cart-list'>
                    <div className='view-cart-list-item'>
                        <div className='view-cart-list-image'>
                            <img alt={title}  src={image} />
                        </div>
                        <div className='view-cart-list-col1'>
                            <div className='view-cart-list-col2'>
                                    <div className='view-cart-list-title'>
                                        {title}
                                    </div>
                                    <div className='view-cart-list-count'>
                                        <i className="fas fa-plus-circle" onClick={()=>{
                                        // dispatch(updateItem({id:id,op:'inc'}))
                                        dispatch(updateItemThunk({id:id,op:'inc'}))
                                    }}></i>
                                        <div className='view-cart-list-items-count-no'>
                                            {count}
                                        </div>
                                        <i className="fas fa-minus-circle" onClick={
                                            ()=>{
                                                // dispatch(updateItem({id:id,op:'dec'}))
                                                dispatch(updateItemThunk({id:id,op:'dec'}))
                                            }
                                        }></i>
                                    </div>
                            </div>
                            <div className='view-cart-list-col3'>
                                <div className='view-cart-list-remove' onClick={
                                        () => {
                                            // dispatch(deleteItem({id}))
                                            dispatch(deleteItemThunk({id}))
                                        }
                                    }>
                                        Remove
                                </div>
                                <div className='view-cart-list-price'>
                                        KWD {totPrice}
                                </div>
                            </div>
                        </div>
                </div>
                <hr/>
            </div>
        )
    })
)
    return (
        <div className='view-cart'>
            {
                cart.items !== '' ? <div className='view-cart-title'>
                            Cart
                      </div>:null
            }
            {/* <div className='view-cart-type'>
                This is a Pickup Order
            </div> */}
            {
                cart.items.toString() === '' ? empty:cartList
            }
            <div className='view-cart-buttons'>
                <Link to='/' className='view-cart-buttons-cont-shop'>
                    Continue Shopping
                </Link>
                { cart.items.toString() !== '' ? <div className='view-cart-buttons-checkout'>
                    Checkout
                </div>:null }
            </div>
        </div>
    )
}