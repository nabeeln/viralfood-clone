import React, { useState,useEffect } from 'react'
import { Link } from 'react-router-dom'
import './Food.css'
import { useDispatch,useSelector } from 'react-redux'
import { addToCartThunk, fetchCart } from '../redux/CartSlice'
import { updateFavorite } from '../redux/FoodSlice'

export default function Food(props){
    // food item count state
    const [ count, setCount ] = useState(1)
    //increment and decrement button handles
    const incrementCount = () => setCount(count+1)
    const decrementCount = () => count >  1 ? setCount(count-1):null
    // get individual food details with the passed id
    // const foodData = ItemsData.find(item=>
    //     item.id===location.state.id
    // )
    const food = useSelector(state => state.food.items)
    const foodData = food.find(item => item.id === props.match.params.id)
    const food_id = foodData.id
    const title = foodData.title
    const des = foodData.des
    const image = foodData.image
    const favorite = foodData.favorite
    const price = foodData.price
    const totPrice = price * count
    //add to cart - redux
    const dispatch = useDispatch()
    const addItems = () => {
        // dispatch(addToCart(title,count,price,food_id,image))
        dispatch(addToCartThunk({title,count,price,food_id,image}))
    }

    useEffect(()=>{
        dispatch(fetchCart())
    },[dispatch])

    return (
        <div className='food-main'>
            <div className='food-image'>
                <img alt={title} src={image} />
            </div>
            <div className='food-info'>
                <div className="food-wish-group">
                    <div className='food-info-title'>
                        {title}
                    </div>
                    <div className="title-wish" onClick={() => dispatch(updateFavorite({id:food_id,favorite:!favorite}))}>
                        {
                            favorite ? <i className="fas fa-heart fa-lg"/>:<i className="far fa-heart fa-lg"/>
                        }
                    </div>
                </div>
                <div className='food-info-des'>
                    {des}
                </div>
                <div className='food-info-price'>
                    Special Price: KWD {price}
                </div>
                <div className='food-info-instruction'>
                    <input 
                        className='food-info-instruction-input' 
                        type='text'
                        placeholder='Add Instruction'    
                    />
                    <hr/>
                </div>
                <div className='food-info-count'>
                    <div  className='food-info-count-inc' onClick={decrementCount}>
                        -
                    </div> 
                    {count} 
                    <div  className='food-info-count-dec' onClick={incrementCount}>
                        +
                    </div>
                </div>
                <Link to='/' className='food-info-add' onClick={addItems}>
                    <div className='food-info-add-price'>
                        KWD {totPrice}
                    </div>
                    <div className='food-info-add-cart'>
                        Add to Cart
                    </div>
                </Link>
            </div>
        </div>
    )
}