import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import './Navbar.css'
import { useSelector, useDispatch} from 'react-redux'
import SignIn from './SignIn'
import {changeSignin} from '../redux/SigninSlice'

function Navbar(){
    //
    const deliveryArea = useSelector(state=>state.delivery_options)
    //cart count
    const cart = useSelector(state=>state.cart) 
    //sign in state
    const signin = useSelector(state => state.signin)
    // navigation inside onclick
    const history = useHistory()
    // hamburger menu state
    const [ click, setClick ] = useState(false)
    //search button state
    const [ search,setSearch ] = useState(false)
    //hamburger menu click handle
    const menuClicked = () => setClick(!click) 
    //sign in status update to redux store
    const dispatch = useDispatch()
    const handleSignin = () => {
        dispatch(changeSignin({open:true}))
        setClick(!click)
    }

/*     useEffect(() => {
        dispatch(fetchCart())
    }, [dispatch]) */
    return (
        <>
            {/* singin view bases on the signin state */}
            { signin.open ? <SignIn/>:null }
            {/* top level header in desktop */}
            <nav className='top-view'>
                <div className='top-view-delivery-text'>
                    Delivery in 30 Mins <i className="fas fa-map-marker-alt"/> {deliveryArea.delivery}
                </div>
                <ul className='top-view-info'>
                    <li className   ='top-view-info-item'>
                        <div className='top-view-info-links'>
                            Schedule Delivery
                        </div>
                    </li>
                    <li className='top-view-info-item'>
                        <div className='top-view-info-links'>
                            <i className="fas fa-phone"></i> Call 
                        </div>
                    </li>
                    <li className='top-view-info-item'>
                        <div className='top-view-info-links'>
                            <i className="fab fa-whatsapp fa-lg"></i> Whatsapp 
                        </div>
                    </li>
                </ul>
            </nav>
            {/* main header section */}
            <nav className='navbar'>
                <Link to='/view-cart' className='cart-icon-link'>
                    <div className='cart-icon'>
                        <i className="fas fa-shopping-cart cart-icon"></i>
                        <div className='cart-icon-count'>{cart.count}</div>
                    </div>
                 </Link>
                <Link to='/' className='navbar-logo'>
                    Taste Bucket Shop
                </Link>
                <div className='menu-icon'>
                    <i className={ click ? 'fas fa-times':'fas fa-bars' } onClick={menuClicked}/>
                </div>
                <ul className={ click ? 'nav-menu active':'nav-menu' }>
                    <li className='nav-item-search'>
                        <div className={ search ? 'nav-links-search-active':'nav-links-search' }>
                            <i className={ search ? 'fas fa-search active':'fas fa-search' } onClick={ () => setSearch(true) }/>
                                <div className={ search ? 'search-input-active':'search-input' }>
                                    <input  
                                        className='search-input-field' 
                                        placeholder='Search Here..'
                                        onChange={ (e) => history.push(`/product-search/${e.target.value}`) }
                                    />
                                    <i className="fas fa-times-circle" onClick={ () => setSearch(false) }></i>
                                </div>
                        </div>
                    </li>
                    <li className='nav-item'>
                        <div className='nav-links' onClick={handleSignin}>
                            Sign In
                        </div>
                    </li>
                    <li className='nav-item'>
                        <Link to='/wishlist' className='nav-links' onClick={menuClicked}>
                            Wish List
                        </Link>
                    </li>
                    <li className='nav-item-branch'>
                        <Link to='/branches' className='nav-links' onClick={menuClicked}>
                            Branches
                        </Link>
                    </li>
                    <li className='nav-item-faq'>
                        <Link to='/faq' className='nav-links' onClick={menuClicked}>
                            FAQ
                        </Link>
                    </li>
                    <li className='nav-item-view-cart'>
                        <Link to='/view-cart' className='nav-links-cart'>
                            <div className='nav-links-cart-icon'>
                                <i className="fas fa-shopping-cart"></i>
                                <div className='cart-count'>{cart.count}</div>
                            </div>
                            <div className='nav-links-cart-total'>KWD {cart.total}</div>
                        </Link>
                    </li>
                </ul>
            </nav>
        </>
    )
}
export default Navbar