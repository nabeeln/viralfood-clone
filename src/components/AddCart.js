import React from 'react'
import './AddCart.css'
import { Link } from 'react-router-dom'
import { useSelector }  from 'react-redux'

export default function AddCart(){
    const cartInfo = useSelector( state => state.cart )
    return (
        <Link to='/view-cart' className='add-cart-button'>
            <div className='add-cart-button-count'>
                {cartInfo.count}
            </div>
            <div className='add-cart-button-price'>
                KWD {cartInfo.total}
            </div>
            <div className='add-cart-button-view'>
                View Cart
            </div>
        </Link>
    )
}