import React from 'react'
import './Form.css'
import { useFormik } from 'formik'
import * as Yup from 'yup'

export default function SignupForm(){
    //formik - sign in form 
    const signinFormik = useFormik({
        initialValues:{
            name:'',
            email:'',
            password:'',
            phone:''
        },
        validationSchema:Yup.object({
            name: Yup.string().min(8,'minimum 8 characters').required('Required'),
            email: Yup.string().email('invalid email').required('Required'),
            password: Yup.string().min(8,'minimum 8 characters needed').required('Required'),
            phone: Yup.number().required('Required')
        }),
        onSubmit: () => {}
    })
    return (
        <form className='form-view' onSubmit={signinFormik.handleSubmit}>
            <div className='form-view-field'>
                <input className='form-view-input' type='text' name='name' value={signinFormik.values.name} onChange={signinFormik.handleChange}/>
                <label className={ signinFormik.values.name ? "Active":"" } htmlFor='name'>
                    <i className="fas fa-user"></i> Name
                </label>
                <hr/>
            </div>
            {/* validation error text */}
            { signinFormik.errors.name && signinFormik.touched.name ? <div className='validate'>*{signinFormik.errors.name}</div>:null }
            <div className='form-view-field'>
                <input className='form-view-input' type='email' name='email' value={signinFormik.values.email} onChange={signinFormik.handleChange}/>
                <label className={ signinFormik.values.email ? "Active":"" } htmlFor='email'>
                    <i className="fas fa-envelope"></i> Email
                </label>
                <hr/>
            </div>
            { signinFormik.errors.email && signinFormik.touched.email ? <div className='validate'>*{signinFormik.errors.email}</div>:null }
            <div className='form-view-field'>
                <input className='form-view-input' type='password' name='password' onChange={signinFormik.handleChange} value={signinFormik.values.password}/>
                <label className={ signinFormik.values.password ? "Active":"" } htmlFor='password'>
                    <i className="fas fa-lock"></i> Password
                </label>
                <hr/>
            </div>
            { signinFormik.errors.password && signinFormik.touched.password ? <div className='validate'>*{signinFormik.errors.password}</div>:null }
            <div className='form-view-field'>
                <input className='form-view-input' type='number' name='phone' onChange={signinFormik.handleChange} value={signinFormik.values.phone}/>
                <label className={ signinFormik.values.phone ? "Active":"" } htmlFor='phone'>
                    <i className="fas fa-mobile-alt"></i> Phone
                </label>
                <hr/>
            </div>
            { signinFormik.errors.phone && signinFormik.touched.phone ? <div className='validate'>*{signinFormik.errors.phone}</div>:null }
            <div className='form-view-button-field'>
                <button type='submit' className='form-view-button'>
                    Sign Up
                </button>
            </div>
        </form>
    )
}