import React, { useEffect,useState } from 'react'
import { useSelector } from 'react-redux'
import './TabNav.css'
import { Link } from 'react-router-dom'
import { Categories } from '../data/ItemsData'
import { FoodCard } from '../components/FoodCard'

// tab navigation including the food items section in the card view
export default function TabNav(){
    const food = useSelector(state=>state.food.items)
    const [ cat,setCategory ] = useState('New')
    const foodList =  food.filter(item => item.categories.find(category => category === cat));
    // sticky navigation tab for card view items
    useEffect(() => {
        const header = document.getElementById("tab-nav");
        const sticky = header.offsetTop;
        // const scrollCallBack = 
        window.addEventListener("scroll", () => {
          if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
            
          } else {
            header.classList.remove("sticky");
            
          }
        });
        /* return () => {
          window.removeEventListener("scroll", scrollCallBack);
        }; */
      }, []);
    return(
        <>
            <nav id="tab-nav" className="tab-nav">
                <ul className='tab-nav-menu'>
                    {
                        Categories.map(item => {
                            return (
                                <li key={item.id} className={ cat===item.title ? 'nav-items-clicked':'nav-items' } onClick={()=>setCategory(item.title)}>
                                    <Link  className='nav-link'>
                                        {item.title}
                                    </Link>
                                </li>
                            )
                        })
                    }
                </ul>
            </nav>
            <div className='card-section'>
                <FoodCard data={foodList} />
            </div>
        </>
    )
}   