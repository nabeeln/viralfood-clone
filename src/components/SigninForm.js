import React from 'react'
import './Form.css'
import { useFormik } from 'formik'
import * as Yup from 'yup'

export default function SigninForm(){
    //formik - sign in form 
    const signinFormik = useFormik({
        initialValues:{
            email:'',
            password:''
        },
        validationSchema: Yup.object({
            email: Yup.string().email('invalid email').required('Required'),
            password: Yup.string().min(8,'minimum 8 characters needed').required('Required')
        }),
        onSubmit: () => {}
    })
    return (
        <form className='form-view' onSubmit={signinFormik.handleSubmit}>
            <div className='form-view-field'>
                <input 
                    className='form-view-input' 
                    type='email' name='email' 
                    value={signinFormik.values.email} 
                    { ...signinFormik.getFieldProps('email') }    
                />
                <label className={ signinFormik.values.email ? "Active":"" } htmlFor='email'>
                    <i className="fas fa-envelope"></i> Email
                </label>
                <hr/>
            </div>
            {/* validation error text */}
            { signinFormik.errors.email && signinFormik.touched.email ? (<div className='validate'>*{signinFormik.errors.email}</div>):null }
            <div className='form-view-field'>
                <input 
                    className='form-view-input' 
                    type='password' name='password' 
                    value={signinFormik.values.password}
                    { ...signinFormik.getFieldProps('password') }
                />
                <label className={ signinFormik.values.password ? "Active":"" } htmlFor='password'>
                    <i className="fas fa-lock"></i> Password
                </label>
                <hr/>
            </div>
            { signinFormik.errors.password && signinFormik.touched.password ? (<div className='validate'>*{signinFormik.errors.password}</div>):null }
            <div className='form-view-button-field'>
                <button type='submit' className='form-view-button'>
                    Sign In
                </button>
            </div>
        </form>
    )
}