import React from 'react'
import './FoodCard.css'
import { addToWishList } from '../redux/FoodSlice'
import { useDispatch,useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import Loading from './Loading'

export function FoodCard(props){
    const loading = useSelector(state => state.food.status)
    const dispatch = useDispatch()
    const history = useHistory()
    const food = props.data
    return(
       loading === 'loading' ? <Loading/>:<div className='card-main'>
       {
           food.map(item => (
               <div  key={item.id} className="card-item" onClick={ () => history.push(`/food/${item.id}`) }>
                   <div className="image">
                       <img alt={item.image} src={item.image}/>
                   </div>
                   <div className="info">
                       <div className="title-wish">
                           <div className="title">
                               {item.title}
                           </div>  
                           <div className="wish-view" onClick={e=>
                               {
                                   e.stopPropagation()
                                   dispatch(addToWishList({id:item.id,favorite:!item.favorite}))
                               }
                           }>
                               { item.favorite ? <i className="fas fa-heart fa-lg"/>:<i className="far fa-heart fa-lg"/> }
                           </div>
                       </div>
                       <div className="des">
                           {item.des.substr(0,70)}
                       </div>
                       <div className="price">
                           KWD {item.price}
                       </div>
                   </div>
               </div>
           ))
       }
   </div>
    )
}