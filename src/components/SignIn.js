import React,{useState} from 'react'
import './SignIn.css'
import { useDispatch } from 'react-redux'
import { changeSignin } from '../redux/SigninSlice'
import SigninForm from '../components/SigninForm'
import SignupForm from '../components/SignupForm'


export default function SignIn(){
    const dispatch = useDispatch()
    const closeSignin = () => dispatch(changeSignin({open:false}))
    //button trigger state
    const [signinClick, setSigninClick] = useState(true)

    return (
        <div className='main'>
            <div className='sign-in'>
                <div className='sign-in-close'>
                    <i className="fas fa-times fa-lg" onClick={closeSignin}></i>
                </div>
                <div className='sign-in-title'>
                    Taste Bucket Shop
                </div>
                <div className='sign-in-buttons'>
                    <div 
                        className={ signinClick ? 'sign-in-buttons-signin-active':'sign-in-buttons-signin'} 
                        onClick={ () => setSigninClick(true) }
                    >
                        Sign In
                    </div>
                    <div 
                        className={ !signinClick ? 'sign-in-buttons-signup-active':'sign-in-buttons-signup'} 
                        onClick={ () => setSigninClick(false) }
                    >
                        Sign Up
                    </div>
                </div>
                <div className='sign-in-buttons-views'>
                   { signinClick ? <SigninForm/>:<SignupForm/> }
                </div>
                <div className='sign-in-footer'/>
            </div>
        </div>
    )
}