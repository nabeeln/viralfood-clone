import React from 'react'
import './Footer.css'

// footer component - wont be visible in mobile view
export default function Footer(){
    return (
            <>
                <div className='footer'>
                <div className='footer-col-1'>
                    
                </div>
                <div className='footer-col-2'>
                    <div className='footer-social'>
                        <div className='footer-social-icon'>
                            <i className="fab fa-facebook fa-lg"></i>
                        </div>
                        <div className='footer-social-icon'>
                            <i className="fab fa-instagram fa-lg"></i>
                        </div>
                        <div className='footer-social-icon'>
                        <i className="fab fa-snapchat fa-lg"></i>
                        </div>
                        <div className='footer-social-icon'>
                            <i className="fab fa-twitter fa-lg"></i>
                        </div>
                    </div>
                    <div className='footer-info'>
                        Powered by Viral
                    </div>
                </div>
                <div className='footer-col-3'>
                    
                </div>
            </div>
            </>
    )
}