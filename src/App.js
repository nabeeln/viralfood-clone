import React,{useEffect} from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import './App.css';
// components
import Navbar from './components/Navbar'
import Footer from './components/Footer'
// pages
import Home from './Pages/Home'
import Branches from './Pages/Branches'
import Food from './Pages/Food'
import FAQ from './Pages/FAQ'
import ViewCart from './Pages/ViewCart'
import DeliveryOptions from './Pages/DeliveryOptions'
import ProductSearch from './Pages/ProductSearch'
import WishList from './Pages/WishList'
//dispatch
import { useDispatch,useSelector } from 'react-redux'
import { fetchFood } from './redux/FoodSlice'
import Loading from './components/Loading';
import { fetchCart } from './redux/CartSlice';

function App() {
  const dispatch = useDispatch()
  const loading = useSelector(state => state.food.status)
  useEffect(() => {
    dispatch(fetchFood())
    dispatch(fetchCart())
  }, [dispatch])
  const content = (
    <div className='app-main'>
          <Navbar/>
          <Switch className='app-switch'>
            <Route path='/' exact component={Home}/>
            <Route path='/branches' component={Branches}/>
            <Route path='/food/:id' component={Food}/>
            <Route path='/food/' component={Food}/>
            <Route path='/faq' component={FAQ}/>
            <Route path='/view-cart' component={ViewCart} />
            <Route path='/delivery-options' component={DeliveryOptions} />
            <Route path='/product-search/:search' component={ProductSearch} />
            <Route path='/product-search/' component={ProductSearch} />
            <Route path='/wishlist' component={WishList} />
          </Switch>
          <Footer/>
    </div>
  )
  return (
    <Router>
      {
        loading === 'loading' ? <Loading/> : content
      }
    </Router>  
);
}

export default App;
