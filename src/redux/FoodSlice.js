import { createSlice,createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

const apiUrl = 'https://60365508c3d42700172e6a8f.mockapi.io/food/'
export const fetchFood = createAsyncThunk('food/loadFood',async () => {
    const food = await axios.get(apiUrl).then(foodData => foodData.data)
    return food
})

export const addToWishList = createAsyncThunk('food/updateFavorite',async (payload,thunk) => {
    const {id,favorite} = payload
    const item = thunk.getState().food.items.find(food => food.id === id)
    await axios.put(apiUrl+id,{...item,favorite:favorite})
    return id
})

const FoodSlice = createSlice({
    name:'food',
    initialState: {
        items:[],
        status:'idle'
    },
    reducers:{},
    extraReducers:builder => {
        builder.addCase(fetchFood.fulfilled,(state,action)=>{
            state.items = action.payload
            state.status = 'idle'
        })
        builder.addCase(fetchFood.pending,(state)=>{
            state.status = 'loading'
        })
        builder.addCase(addToWishList.fulfilled,(state,action)=>{
            const item = state.items.find(food => food.id === action.payload)
            if(item){
                item.favorite = !item.favorite
            }
        })
    }
})

export default FoodSlice.reducer