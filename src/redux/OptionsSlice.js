import { createSlice } from '@reduxjs/toolkit'

const area = {
    delivery: '',
    pickup: '',
    defaultOption: 'Delivery'
}

const OptionsSlice = createSlice({
    name:'delivery_options',
    initialState:area,
    reducers:{
        addArea: (state, action) => {
            state.defaultOption = action.payload.defaultOption
            if(action.payload.defaultOption === 'Delivery'){
                state.delivery = action.payload.area
            } else{
                    state.pickup = action.payload.area
            }
        }
    }
})

export const { addArea } = OptionsSlice.actions
export default OptionsSlice.reducer