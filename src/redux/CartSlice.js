import { createSlice,createAsyncThunk } from '@reduxjs/toolkit'
import { nanoid } from '@reduxjs/toolkit'
import axios from 'axios'

const fetchUrl = 'https://60365508c3d42700172e6a8f.mockapi.io/cart/'
const apiUrl = 'https://60365508c3d42700172e6a8f.mockapi.io/cart/1'

const cartData =
    {
        items: [],
        count: 0,
        total: 0
    }

export const fetchCart = createAsyncThunk('cart/fetchCart',async () => {
    const cartData = await axios.get(fetchUrl).then(foodData => foodData.data)
    return cartData
})


export const addToCartThunk = createAsyncThunk('cart/addToCartThunk',async (cartItem,ThunkAPI) => {
    const state = ThunkAPI.getState().cart
    var cartState ={}
    const { 
        title, 
        count, 
        price, 
        food_id,
        image 
    } = cartItem

    const id = nanoid()

    var itemPrice = price*count

    var totCount = state.count + count

    const item = { 
        id:id,
        itemName: title,
        count:count,
        price:price,
        totPrice:itemPrice,
        food_id:food_id,
        image:image 
    }

    var foodItem = state.items.find(item => item.food_id === food_id)

    if(foodItem){
        const newItems = [...state.items]
        newItems.splice(newItems.indexOf(foodItem),1)

        foodItem = { 
            ...foodItem,
            count: foodItem.count+count,
            totPrice:itemPrice+price
        }

        cartState = {
            ...state,
            items:[...newItems,foodItem]
        }

        await axios.put(apiUrl, cartState);
        ThunkAPI.dispatch(addToCart(cartState))
    } else{
        cartState = {
            ...state,
            items:[
                ...state.items,
                item
            ]}
        await axios.put(apiUrl,cartState);
        ThunkAPI.dispatch(addToCart(cartState))
    }
    
    await axios.put(apiUrl,{...cartState,count:totCount,total:itemPrice + state.total})
    return {...cartState,count:totCount,total:itemPrice + state.total}
})


export const updateItemThunk = createAsyncThunk('cart/updateItemThunk',async (payload,ThunkAPI) => {
    const {
        id,
        op
    } = payload

    var cartState = {}

    const state = ThunkAPI.getState().cart

    var foodItem = state.items.find( item => item.id === id )

    if(foodItem){
        if(op === 'inc'){
            const newItems = state.items.map(item => item.id === id ? item = {...item,count:item.count+1,totPrice:item.totPrice+item.price}:item)

            cartState = {
                ...state,
                items:[
                    ...newItems
                ],
                count: state.count + 1,
                total: state.total + foodItem.price
            }

            await axios.put(apiUrl,cartState)
            ThunkAPI.dispatch(updateItem(cartState))
        } else if( op === 'dec' ){
            if(foodItem.count > 1){
                const newItems = state.items.map(
                    item => item.id === id ? item = {...item,count:item.count-1,totPrice:item.totPrice-item.price}:item
                )

                cartState = {
                    ...state,
                    items:[
                        ...newItems
                    ],
                    count: state.count - 1,
                    total: state.total - foodItem.price
                }

                await axios.put(apiUrl,cartState)
                ThunkAPI.dispatch(updateItem(cartState))
            } else{
                cartState = state
            }
        }
        return cartState

    }
})

export const deleteItemThunk = createAsyncThunk('cart/deleteItemThunk',async (payload,ThunkAPI) => {
    const {id} = payload

    const state = ThunkAPI.getState().cart

    var foodItem = state.items.find( item => item.id === id )
    
    if(foodItem){
        const newItems = [...state.items]
        newItems.splice(newItems.indexOf(foodItem),1)
        const Data = {
            items:newItems,
            count:state.count-foodItem.count,
            total:state.total-foodItem.totPrice
        }
        await axios.put(apiUrl,Data)
        return Data
    }
})

const CartSlice = createSlice({
    name:'cart',
    initialState: cartData,
    reducers:{
        addToCart:(state,action) => {
                state.items = action.payload.items
        },
        updateItem: (state,action) => {
            state.items = action.payload.items
        }
    },
    extraReducers: builder => {
        builder.addCase(fetchCart.fulfilled,(state,action) => {
            const cartData = action.payload
            state.count = cartData[0].count
            state.total = cartData[0].total
            state.items = cartData[0].items
        })
        builder.addCase(addToCartThunk.fulfilled,(state,action) => {
            state.count = action.payload.count
            state.total = action.payload.total
            state.items = action.payload.items
        }) 
        builder.addCase(updateItemThunk.fulfilled,(state,action) => {
            state.count = action.payload.count
            state.total = action.payload.total
        })
        builder.addCase(deleteItemThunk.fulfilled,(state,action) => {
            state.items = action.payload.items
            state.count = action.payload.count
            state.total = action.payload.total
        })
    }
})

export const { addToCart,deleteItem, updateItem ,updateFavorite} = CartSlice.actions
export default CartSlice.reducer