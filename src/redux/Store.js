import { configureStore } from '@reduxjs/toolkit'
import CartSlice from './CartSlice'
import OptionsSlice from './OptionsSlice'
import SigninSlice from './SigninSlice'
import FoodSlice from './FoodSlice'

export default configureStore({
    reducer:{
        cart: CartSlice,
        delivery_options: OptionsSlice,
        signin: SigninSlice,
        food:FoodSlice
    }
})