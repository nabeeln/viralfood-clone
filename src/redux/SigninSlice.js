import { createSlice } from '@reduxjs/toolkit'

const signinView = {
    open: false
}

const SigninSlice = createSlice({
    name:'signin',
    initialState:signinView,
    reducers:{
        changeSignin: (state,action)=>{
            state.open = action.payload.open
        }
    }
})
export const { changeSignin } = SigninSlice.actions
export default SigninSlice.reducer